# -*- coding: utf-8 -*-
__author__ = 'CSH'

import sys
import itertools
import random
import copy
from PyQt4 import QtGui, QtCore, uic

# white - top, black - bottom


class Figure(QtGui.QLabel):
    def __init__(self, parent, color):
        QtGui.QLabel.__init__(self, parent)
        #self.setTeamColor(color)

        super().setPixmap(QtGui.QPixmap('image/' + str(self) + "_" + color + '.png'))
        self.color = color

        self.isActive = False

        self.words = "ABCDEFGH"
        self.ints = ["I", "II", "III", "IV", "V", "VI", "VII", "VIII"]

        self.Pos = property(self.getPos, self.setPos)
        self.PosX = property(self.getPosX, self.setPosX)
        self.PosY = property(self.getPosY, self.setPosY)
    #def setTeamColor(self, color_):


    def teamColor(self):
        return self.color

    def actives(self):
        if self.parent().nowTurn == self.teamColor() and not self.parent().isSomePeshkaTop():
            self.setStatus(not self.Status())
            try:
                #print(self.getAllowSteps())
                self.parent().clearHelpLabel()
                if self.Status():
                    self.parent().showHelpLabel(self.getAllowSteps())
            except:
                print("Еще не задано проверок")
        elif self.parent().nowTurn != self.teamColor():
            self.parent().showWarnBanner()

    def mousePressEvent(self, *args, **kwargs):
        self.actives()


    def setStatus(self, status):
        if status:
            self.parent().removeActivity()
        self.isActive = status

        if self.isActive:
            self.setStyleSheet("background: url('image/splash_" + str(random.randint(1, 5)) + ".png');")
        else:
            self.setStyleSheet("")

    def Status(self):
        return self.isActive

    def setPos(self, val):
        self.setGeometry(val[0], val[1], 74, 74)
    #real pos, X&Y
    def getPos(self):
        return [self.x(), self.y()]

    def setPosX(self, val):
        self.setGeometry(val, self.y(), 74, 74)
    #real pos, X&Y
    def getPosX(self):
        return self.x()

    def setPosY(self, val):
        self.setGeometry(self.x(), val, 74, 74)
    #real pos, X&Y
    def getPosY(self):
        return self.y()

    def setPosFromAddres(self, addres):
        x_ = 0
        y_ = 0

        if addres[0] in self.words:
            y_ = self.words.find(addres[0]) * 100 + 63

        addres = addres.replace(addres[0], "")

        for i in range(0, len(self.ints)):
            if self.ints[i] == addres:
                y_ = i * 100 + 63

        self.setGeometry(x_, y_, 74, 74)
    #координаты позиции, A-VI, B-II и пр
    def PosFromAddres(self):
        return [self.ints[(self.x() - 50)//100], self.words[(self.y() - 50)//100]]

    def setPosFromXY(self, x, y):
        self.setGeometry(x * 100 + 63, y * 100 + 63, 74, 74)
    #относительные координаты, 0-1, 0-0, 4-7, 7-7 (номер клетки)
    def PosFromXY(self):
        return [(self.x() - 50)//100, (self.y() - 50)//100]


class MoveBlock(QtGui.QLabel):
    def __init__(self, parent, position):
        QtGui.QLabel.__init__(self, parent)

        self.position = position
        self.isFoe = False

        if self.parent().isAnyoneHere(self.PosFromXY()[0], self.PosFromXY()[1], 'xy')[0]:
            self.setPixmap(QtGui.QPixmap("image/step_attack.png"))
            self.isFoe = True
        else:
            self.setPixmap(QtGui.QPixmap("image/step_free.png"))

    def mousePressEvent(self, QMouseEvent):
        self_pos = self.PosFromXY()
        isOk = self.parent().moveActive(self_pos[0], self_pos[1])
        self.parent().isOk = isOk
        self.parent().changeTurn()

    def Pos(self):
        return [self.x(), self.y()]

    #координаты позиции, A-VI, B-II и пр
    def PosFromAddres(self):
        return [self.ints[(self.position[0] - 50)//100], self.words[(self.position[1] - 50)//100]]

    #относительные координаты, 0-1, 0-0, 4-7, 7-7 (номер клетки)
    def PosFromXY(self):
        return [(self.position[0] - 50)//100, (self.position[1] - 50)//100]


class Tura(Figure):
    def __init__(self, parent, color):
        Figure.__init__(self, parent, color)

    def __str__(self):
        return "tura"

    def getAllowSteps(self):
        myPos = self.PosFromXY()
        allowSteps = []

        addit_mass = [[0, 1], [0, -1], [1, 0], [-1, 0]]

        for i in addit_mass:
            multiple = 0
            all_correct = True
            while all_correct:
                multiple += 1#HAVE MNOGO ERROR!

                x_ = myPos[0] + (i[0] * multiple)
                y_ = myPos[1] + (i[1] * multiple)

                IAH = self.parent().isAnyoneHere(x_ , y_, "xy")

                if not IAH[0] and x_ >= 0 and x_ <= 7 and y_ >= 0 and y_ <= 7:
                    allowSteps.append([x_, y_])
                elif IAH[0] and IAH[1].teamColor() != self.teamColor():
                    allowSteps.append([x_, y_])
                    all_correct = False
                else:
                    all_correct = False

        return allowSteps


class Officer(Figure):
    def __init__(self, parent, color):
        Figure.__init__(self, parent, color)

    def __str__(self):
        return "officer"

    def getAllowSteps(self):
        myPos = self.PosFromXY()
        allowSteps = []

        addit_mass = [[1, 1], [1, -1], [-1, 1], [-1, -1]]

        for i in addit_mass:
            multiple = 0
            all_correct = True
            while all_correct:
                multiple += 1

                x_ = myPos[0] + (i[0] * multiple)
                y_ = myPos[1] + (i[1] * multiple)

                IAH = self.parent().isAnyoneHere(x_ , y_, "xy")

                if not IAH[0] and x_ >= 0 and x_ <= 7 and y_ >= 0 and y_ <= 7:
                    allowSteps.append([x_, y_])
                elif IAH[0] and IAH[1].teamColor() != self.teamColor():
                    allowSteps.append([x_, y_])
                    all_correct = False
                else:
                    all_correct = False

        return allowSteps


class Peshka(Figure):
    def __init__(self, parent, color):
        Figure.__init__(self, parent, color)

    def getAllowSteps(self):
        myPos = self.PosFromXY()
        allowSteps = []

        if self.teamColor() == "white":
            # min_pos = 1
            # проверимпервый ход пешки - на 2 "вперед"
            if myPos[1] == 1:
                if not self.parent().isAnyoneHere(myPos[0], 3, "xy")[0]:
                    allowSteps.append([myPos[0], 3])
            # теперь проверим обычный ход пешки - на 1 вперед
            if (myPos[1] + 1 < 8) and (not self.parent().isAnyoneHere(myPos[0], myPos[1]+1, "xy")[0]):
                allowSteps.append([myPos[0], myPos[1] + 1])
            # Проверим рубку по диагонали
            if myPos[0] > 0:
                IAH = self.parent().isAnyoneHere(myPos[0] - 1, myPos[1] + 1, "xy")

                if IAH[0] and IAH[1].teamColor() != self.teamColor():
                    allowSteps.append([myPos[0] - 1, myPos[1] + 1])
            if myPos[0] < 7:
                IAH = self.parent().isAnyoneHere(myPos[0] + 1, myPos[1] + 1, "xy")

                if IAH[0] and IAH[1].teamColor() != self.teamColor():
                    allowSteps.append([myPos[0] + 1, myPos[1] + 1])
        else:
            # max_pos = 6
            # проверимпервый ход пешки - на 2 "вперед"
            if myPos[1] == 6:
                if not self.parent().isAnyoneHere(myPos[0], 4, "xy")[0]:
                    allowSteps.append([myPos[0], 4])
            # теперь проверим обычный ход пешки - на 1 вперед
            if (myPos[1] - 1 < 8) and (not self.parent().isAnyoneHere(myPos[0], myPos[1] - 1, "xy")[0]):
                allowSteps.append([myPos[0], myPos[1] - 1])
            # Проверим рубку по диагонали
            if myPos[0] > 0:
                IAH = self.parent().isAnyoneHere(myPos[0] - 1, myPos[1] - 1, "xy")

                if IAH[0] and IAH[1].teamColor() != self.teamColor():
                    allowSteps.append([myPos[0] - 1, myPos[1] - 1])
            if myPos[0] < 7:
                IAH = self.parent().isAnyoneHere(myPos[0] + 1, myPos[1] - 1, "xy")

                if IAH[0] and IAH[1].teamColor() != self.teamColor():
                    allowSteps.append([myPos[0] + 1, myPos[1] - 1])

        return allowSteps

    def __str__(self):
        return "peshka"


class Horse(Figure):
    def __init__(self, parent, color):
        Figure.__init__(self, parent, color)

    def __str__(self):
        return "horse"

    def getAllowSteps(self):
        myPos = self.PosFromXY()
        allowSteps = []

        for i in itertools.product([1, -1],[2, -2]):
            IAH = self.parent().isAnyoneHere(myPos[0] + i[0], myPos[1] + i[1], "xy")
            if not IAH[0]:
                if (myPos[0] + i[0]) >= 0 and (myPos[0] + i[0]) <= 7 and (myPos[1] + i[1]) >= 0 and (myPos[1] + i[1]) <= 7:
                    allowSteps.append([myPos[0] + i[0], myPos[1] + i[1]])
            elif IAH[0] and IAH[1].teamColor() != self.teamColor():
                allowSteps.append([myPos[0] + i[0], myPos[1] + i[1]])

        for i in itertools.product([2, -2],[1, -1]):
            IAH = self.parent().isAnyoneHere(myPos[0] + i[0], myPos[1] + i[1], "xy")
            if not IAH[0]:
                if (myPos[0] + i[0]) >= 0 and (myPos[0] + i[0]) <= 7 and (myPos[1] + i[1]) >= 0 and (myPos[1] + i[1]) <= 7:
                    allowSteps.append([myPos[0] + i[0], myPos[1] + i[1]])
            elif IAH[0] and IAH[1].teamColor() != self.teamColor():
                allowSteps.append([myPos[0] + i[0], myPos[1] + i[1]])

        return allowSteps


class Korol(Figure):
    def __init__(self, parent, color):
        Figure.__init__(self, parent, color)

    def __str__(self):
        return "korol"

    def getAllowSteps(self):
        myPos = self.PosFromXY()
        allowSteps = []

        for i in itertools.product([0, 1, -1], [0, 1, -1]):
            IAH = self.parent().isAnyoneHere(myPos[0] + i[0], myPos[1] + i[1], "xy")
            if not IAH[0] and i != (0, 0):
                if (myPos[0] + i[0]) >= 0 and (myPos[0] + i[0]) <= 7 and (myPos[1] + i[1]) >= 0 and (myPos[1] + i[1]) <= 7:
                    allowSteps.append([myPos[0] + i[0], myPos[1] + i[1]])
            elif IAH[0] and IAH[1].teamColor() != self.teamColor() and i != (0, 0):
                allowSteps.append([myPos[0] + i[0], myPos[1] + i[1]])

        return allowSteps


class Koroleva(Figure):
    def __init__(self, parent, color):
        Figure.__init__(self, parent, color)

    def __str__(self):
        return "koroleva"

    def getAllowSteps(self):
        myPos = self.PosFromXY()
        allowSteps = []

        addit_mass = [[1, 1], [1, -1], [-1, 1], [-1, -1], [0, 1], [0, -1], [1, 0], [-1, 0]]

        for i in addit_mass:
            multiple = 0
            all_correct = True
            while all_correct:
                multiple += 1

                x_ = myPos[0] + (i[0] * multiple)
                y_ = myPos[1] + (i[1] * multiple)

                IAH = self.parent().isAnyoneHere(x_ , y_, "xy")

                if not IAH[0] and x_ >= 0 and x_ <= 7 and y_ >= 0 and y_ <= 7:
                    allowSteps.append([x_, y_])
                elif IAH[0] and IAH[1].teamColor() != self.teamColor():
                    allowSteps.append([x_, y_])
                    all_correct = False
                else:
                    all_correct = False

        return allowSteps


# ----------------------------------------


class SelectableFigures(QtGui.QLabel):
    def __init__(self, parent, name):
        QtGui.QLabel.__init__(self, parent)
        self.name = name
        self.setStyleSheet("SelectableFigures:hover {background: url('image/splash_" + str(random.randint(1, 5)) + ".png');}")

    def mousePressEvent(self, QMouseEvent):
        self.parent().parent().changeTopPeshka(self.name)
        self.parent().parent().changeTurn()
        self.parent().close()


class DialogPeshki(QtGui.QWidget):
    def __init__(self, parent=None):
        super(DialogPeshki, self).__init__(parent)
        self.setGeometry(self.parent().width()/2 - 255, self.parent().height()/2 - 150, 510, 300)
        uic.loadUi("dp.ui", self)

        self.bgpic = QtGui.QLabel(self)
        self.bgpic.setGeometry(0, 0, 510, 300)
        self.bgpic.setPixmap(QtGui.QPixmap("image/dialog.png"))

        self.tura = SelectableFigures(self, 'tura')
        self.tura.setGeometry(30, 128, 74, 74)
        self.tura.setPixmap(QtGui.QPixmap("image/tura_" + str(self.parent().topPeshka) + ".png"))

        self.officer = SelectableFigures(self, 'officer')
        self.officer.setGeometry(148, 118, 74, 74)
        self.officer.setPixmap(QtGui.QPixmap("image/officer_" + str(self.parent().topPeshka) + ".png"))

        self.koroleva = SelectableFigures(self, 'koroleva')
        self.koroleva.setGeometry(278, 118, 74, 74)
        self.koroleva.setPixmap(QtGui.QPixmap("image/koroleva_" + str(self.parent().topPeshka) + ".png"))

        self.horse = SelectableFigures(self, 'horse')
        self.horse.setGeometry(402, 126, 74, 74)
        self.horse.setPixmap(QtGui.QPixmap("image/horse_" + str(self.parent().topPeshka) + ".png"))


class Desk(QtGui.QWidget):
    def __init__(self, parent=None):
        QtGui.QWidget.__init__(self, parent)
        uic.loadUi("form.ui", self)

        self.turnCountButton = QtGui.QPushButton(self)
        self.turnCountButton.setGeometry(0, 0, 50, 50)
        css_text = "background: darkgray; color: white; font-size: 20px; "
        self.turnCountButton.setStyleSheet(css_text)
        self.turnCountButton.setText("0")

        self.setFixedSize(self.width(), self.height())

        temp_Button = QtGui.QPushButton(self)
        temp_Button.setGeometry(50, 0, 200, 25)
        temp_Button.setText("Текущий ход")
        temp_Button.setStyleSheet(css_text)

        self.backTurnButton = QtGui.QPushButton(self)
        self.backTurnButton.setGeometry(0, 50, 25, 100)
        css_active = "background: seagreen; color: white; font-size: 20px; "
        self.backTurnButton.setStyleSheet(css_active)
        self.backTurnButton.setIcon(QtGui.QIcon("image/backArrow.png"))
        self.connect(self.backTurnButton, QtCore.SIGNAL("clicked()"), self.backMove)

        self.temp_tab = QtGui.QPushButton(self)
        self.temp_tab.setGeometry(350, 0, 200, 25)
        self.temp_tab.setText("0:0")
        self.temp_tab.setStyleSheet(css_text)

        #self.timerButton = QtGui.QPushButton(self)
        #self.timerButton.setGeometry(650, 0, 200, 25)
        #self.timerButton.setStyleSheet(css_text)

        self.desk_label.setPixmap(QtGui.QPixmap("desk.png"))

        self.allFigures = []
        self.addresButton = []

        self.helpLabels = []

        self.figure_buttons = []
        for c in range(0, 2):
            for f in range(0, 16):
                temp_butt = QtGui.QPushButton(self)
                temp_butt.setGeometry(50 + (25 * f) + (c * 400), 875, 25, 25)
                temp_butt.setStyleSheet(css_text)
                self.figure_buttons.append(temp_butt)

        #self.fillFigureButtons()
        self.fillAddres()
        self.fillTable()

        self.nowTurn = 'black'

        self.turnCount = 0
        self.isBlackShah = False
        self.isWhiteShah = False

        self.gameLog = []

        self.banner = QtGui.QLabel(self)
        self.banner_two = QtGui.QLabel(self)

        self.topPeshka = "none"

        self.isOk = True
        #
        #global qqw
        #self.topPeshka = "white"
        #qqw = DialogPeshki(self)
        #qqw.show()
        #

        self.changeTurn()

    def fillFigureButtons(self):
        css_text = "background: darkgray; color: white; font-size: 20px; "
        figures = ["tura", "tura", "horse", "horse", "officer", "officer", "koroleva", "korol"]

        white_figures = []
        black_figures = []

        for f in self.allFigures:
            if f.teamColor() == "white":
                white_figures.append(f)
            elif f.teamColor() == "black":
                black_figures.append(f)

        white_list = sorted(white_figures.copy(), key=self.sortPoints)
        black_list = sorted(black_figures.copy(), key=self.sortPoints)

        for i in range(0, 16):
            if i < len(white_list):
                self.figure_buttons[i].setIcon(QtGui.QIcon("image/" + str(white_list[i]) + "_white.png"))
            else:
                self.figure_buttons[i].setIcon(QtGui.QIcon("image/cross.png"))

        for i in range(0, 16):
            if i < len(black_list):
                self.figure_buttons[i + 16].setIcon(QtGui.QIcon("image/" + str(black_list[i]) + "_black.png"))
            else:
                self.figure_buttons[i + 16].setIcon(QtGui.QIcon("image/cross.png"))

    def sortPoints(self, figurs):
        return(str(figurs.teamColor()) + "_" + str(figurs))

    def isSomePeshkaTop(self):
        for f in self.allFigures:
            if str(f) == 'peshka' and (f.PosFromXY()[1] == 0 or f.PosFromXY()[1] == 7):
                self.topPeshka = f.teamColor()
                return True
        return False

    def changeTopPeshka(self, changeTo):
        classes = {"tura": Tura, "officer": Officer, "horse": Horse, "koroleva": Koroleva}
        for f in self.allFigures:
            if str(f) == 'peshka' and (f.PosFromXY()[1] == 0 or f.PosFromXY()[1] == 7):
                new_figure = classes[changeTo](self, f.teamColor())
                new_figure.setPosFromXY(f.PosFromXY()[0], f.PosFromXY()[1])
                new_figure.show()
                self.allFigures.append(new_figure)
                f.hide()
                self.allFigures.remove(f)
                break

    def recountTab(self):
        whites = 0
        blacks = 0
        for f in self.allFigures:
            if f.teamColor() == "white":
                whites += 1
            elif f.teamColor() == "black":
                blacks += 1

        whites = 16 - whites
        blacks = 16 - blacks
        self.temp_tab.setText(str(blacks) + ":" + str(whites))

    def changeTurn(self):
        if self.isSomePeshkaTop():
            qqw = DialogPeshki(self)
            qqw.show()
        else:
            self.turnCount += 1
            print("Ход: " + str(self.turnCount))
            self.addToHistory()

            self.recountTab()
            self.fillFigureButtons()
            self.turnCountButton.setText(str(self.turnCount))

            if self.nowTurn == 'white':
                self.nowTurn = 'black'
            else:
                self.nowTurn = 'white'

            self.showBanner()
            if not self.isOk:
                self.isOk = True
                self.backMove()


    def showBanner(self):
        self.banner.hide()
        temp = QtGui.QLabel(self)
        if self.isWhiteShah or self.isBlackShah:
            temp.setPixmap(QtGui.QPixmap("image/shah.png"))
        else:
            temp.setPixmap(QtGui.QPixmap("image/" + self.nowTurn + "_step_full.png"))
        temp.setGeometry(self.width()/2 - 300, self.height()/2 - (450/2), 600, 450)
        temp.show()
        self.banner = temp

        QtCore.QTimer.singleShot(300, self.hideStepBanner) # GOTO: 1-> 1000

    def showWarnBanner(self):
        self.banner_two.hide()
        temp = QtGui.QLabel(self)
        if self.nowTurn == "white":
            temp.setPixmap(QtGui.QPixmap("image/hey_white.png"))
        elif self.nowTurn == "black":
            temp.setPixmap(QtGui.QPixmap("image/hey_black.png"))
        temp.setGeometry(self.width()/2 - 300, self.height()/2 - (450/2), 600, 450)
        temp.show()
        self.banner_two = temp

        QtCore.QTimer.singleShot(600, self.hideStepBanner_two) # GOTO: 1-> 1000

    def addToHistory(self):
        new_figures = []
        new_posities = []
        for f in self.allFigures:
            x = f
            new_figures.append(x)
            new_posities.append(x.PosFromXY())

        new_log = {'turn': self.turnCount, 'map': new_figures, 'poses': new_posities}
        self.gameLog.append(new_log)

    def hideStepBanner(self):
        self.banner.hide()

    def hideStepBanner_two(self):
        self.banner_two.hide()

    def fillAddres(self):
        for i in range(0, 8):
            for j in range(0, 2):
                temp_button = QtGui.QPushButton(self)
                temp_button_copy = QtGui.QPushButton(self)

                if j == 0:
                    textz = "ABCDEFGH"
                    temp_button.setGeometry(25, (100*i) + 50, 25, 100)
                    temp_button_copy.setGeometry(850, (100*i) + 50, 25, 100)
                else:
                    textz = ["I", "II", "III", "IV", "V", "VI", "VII", "VIII"]
                    temp_button.setGeometry((100*i) + 50, 25, 100, 25)
                    temp_button_copy.setGeometry((100*i) + 50, 850, 100, 25)

                temp_button.setText(textz[i])
                temp_button_copy.setText(textz[i])

                css_text = "background: darkgray; color: white; font-size: 20px; "

                temp_button.setStyleSheet(css_text)
                temp_button_copy.setStyleSheet(css_text)

                self.addresButton.append(temp_button)

    def fillTable(self):
        figures = [Tura, Horse, Officer, Korol, Koroleva, Officer, Horse, Tura]

        for i in range(0, 8):
            for j in range(0, 2):
                if j == 0:
                    temp_peshka = Peshka(self, "white")
                else:
                    temp_peshka = Peshka(self, "black")
                temp_peshka.setPosFromXY(i, (j * 5) + 1)
                self.allFigures.append(temp_peshka)

        for i in range(0, len(figures)):
            for j in range(0, 2):
                if j == 0:
                    temp_figure = figures[i](self, "white")
                else:
                    temp_figure = figures[i](self, "black")
                temp_figure.setPosFromXY(i, (j * 7))
                self.allFigures.append(temp_figure)

    def removeActivity(self):
        for i in self.allFigures:
            i.setStatus(False)

    def isAnyoneHere(self, x, y, typeOfPos):
        isFind = False
        whoIsHere = 0

        for figure in self.allFigures:
            if typeOfPos.lower() == "xy":
                if figure.PosFromXY() == [x, y]:
                    isFind = True
                    whoIsHere = figure
            elif typeOfPos.lower() == "real":
                if figure.Pos() == [x, y]:
                    isFind = True
                    whoIsHere = figure

        return [isFind, whoIsHere]

    def showHelpLabel(self, elems):
        for i in elems:
            tempLabel = MoveBlock(self, [i[0] * 100 + 50, i[1] * 100 + 50])
            tempLabel.setGeometry(i[0] * 100 + 50, i[1] * 100 + 50, 100, 100)
            tempLabel.show()
            self.helpLabels.append(tempLabel)

    def clearHelpLabel(self):
        for i in self.helpLabels:
            i.hide()
            i.destroy()
        self.helpLabels = []

    def moveActive(self, x, y):
        IAH = self.isAnyoneHere(x, y, 'xy')
        if IAH[0]:
            IAH[1].hide()
            for i in self.allFigures:
                if i == IAH[1]:
                    self.allFigures.remove(i)
                    break

        for figure in self.allFigures:
            if figure.Status():
                figure.setStatus(False)
                figure.setPosFromXY(x, y)
                self.clearHelpLabel()

        self.checkShah()
        if (self.isBlackShah and self.nowTurn == 'black') or (self.isWhiteShah and self.nowTurn == 'white'):
            return False
        return True

    def checkShah(self):
        isWhiteShahed = False
        isBlackShahed = False
        for f in self.allFigures:
            steps = f.getAllowSteps()
            for step in steps:
                IAH = self.isAnyoneHere(step[0], step[1], 'xy')
                if IAH[0]:
                    if str(IAH[1]) == 'korol':
                        if IAH[1].teamColor() == 'white':
                            self.isWhiteShah = True
                            isWhiteShahed = True
                        elif IAH[1].teamColor() == 'black':
                            self.isBlackShah = True
                            isBlackShahed = True
                        f.setStyleSheet("background: yellow; border-radius: 35px;")
                        IAH[1].setStyleSheet("background: yellow; border-radius: 35px;")

        if not isWhiteShahed:
            self.isWhiteShah = False
        if not isBlackShahed:
            self.isBlackShah = False

    def backMove(self):
        if self.turnCount > 1:
            for f in self.allFigures:
                f.hide()


            for log in self.gameLog:
                if log['turn'] == self.turnCount - 1:
                    self.allFigures = []
                    for l in range(0, len(log['map'])):
                        log['map'][l].setPosFromXY(log['poses'][l][0],log['poses'][l][1])
                        self.allFigures.append(log['map'][l])

            for f in self.allFigures:
                f.show()

            for log in self.gameLog:
                if log['turn'] == self.turnCount:
                    self.gameLog.remove(log)
                    break

            self.turnCount -= 1
            if self.turnCount % 2 == 1:
                self.nowTurn = 'white'
            else:
                self.nowTurn = 'black'
            self.showBanner()
            self.turnCountButton.setText(str(self.turnCount))
            self.removeActivity()
            self.checkShah()
            self.recountTab()
            self.fillFigureButtons()
            #self.checkPeshka()
    '''
    def paintEvent(self, event):
        self.paint_ = self.parent()
        self.paint_.setPen(QtGui.QPen(QtGui.QColor("#0F0"), 1, QtCore.Qt.SolidLine, QtCore.Qt.RoundCap))
        self.paint_.setBrush(QtGui.QBrush(QtGui.QColor("#070"), QtCore.Qt.SolidPattern)) #fc6c2d
        self.paint_.setRenderHint(QtGui.QPainter.Antialiasing)

        print(1)
        for l in self.helpLines:
            print(l)
            #self.paint_.drawRect(l[0] * 100 + 63, l[1] * 100 + 63, 74, 74)
            self.paint_.drawRect(0, 0, 74, 74)

        self.paint_.end()
    '''


app = QtGui.QApplication(sys.argv)
window = Desk()
window.show()
sys.exit(app.exec_())
